/*
* This program calls the vote POST request to make a vote.
* A POST request requires a body (data). This data is your
* vote.
* The return value is a JSON object.
* FEEDBACK: return 0 for success, non-zero for error
*/

//client_vote.js
var http = require('http');
//get host IP and port # from command line
var myArgs = process.argv.slice(2);
HOST_IP = myArgs[0];
PORT_NUM = myArgs[1];
SUCCESS = 0

/*
request options JSON
host - IP address of server computer
port - port where service is running
path - route of service you are trying to reach
method - type of HTTP service
headers - specify that we are sending a JSON form as data
*/
var options = {
  'host': HOST_IP,
  'port': PORT_NUM,
  path: '/vote',
  method: 'POST',
  headers: {'Content-Type': 'application/json'}
};

//form data (a JSON object)
var data = { "vote": "m&ms" }; //change to "m&ms" if you want

//send the POST request
var req = http.request(options, readResponse);
req.write(JSON.stringify(data)); //convert JSON to a string before sending
req.on('error', function(err) {
  console.log('ERR: ' + err);
  console.log('Error caught: ' + err.errno); //non-zero is an error code
  process.exit(err.errno);
});
req.end();

/*
readResponse() is called when the POST response is received.
The response may be in separate chunks.
The function stores the final response message in responseData.
The 'data' event receives multiple chunks of data.
The 'end' event means there are no more chunks, and it has the final responseData.
*/
function readResponse(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
    var dataObj = JSON.parse(responseData); //convert response string to a JSON object
    console.log("Complete response: " + dataObj.msg); //output just the msg field of the JSON response
    process.exit(SUCCESS); //0 is success
  });
}
