/*
* This program calls the hello GET request to simply see if the server is active.
* The return value is a JSON object.
* FEEDBACK: return 0 for success, non-zero for error
*/

var http = require('http');
//get host IP and port # from command line
var myArgs = process.argv.slice(2);
HOST_IP = myArgs[0];
PORT_NUM = myArgs[1];
SUCCESS = 0

/*
request options JSON
host - IP address of server computer
port - port where service is running
path - route of service you are trying to reach
method - type of HTTP service
*/
var options = {
  'host': HOST_IP,
  'port': PORT_NUM,
  'path': '/foo',     
  'method': 'GET'
};

//send the GET request
var req = http.request(options, responseRcvd);
req.on('error', function(err) {
  console.log('ERR: ' + err);
  console.log('Error caught: ' + err.errno); //non-zero is an error code
  process.exit(err.errno);
});
req.end();

/*
responseRcvd() is called when the GET response is received.
The response may be in separate chunks.
The function stores the final response message in responseData.
The 'data' event receives multiple chunks of data.
The 'end' event means there are no more chunks, and it has the final responseData.
*/
function responseRcvd(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
    obj = JSON.parse(responseData);
    if (obj.msg == 'bar') {
      console.log('SUCCESS!');
      process.exit(SUCCESS); //0 is success
    } else {
      console.log('ERROR!');
      process.exit(-1); //-1 error
    }
    
  });
}
