/*
* This program calls the hello GET request to simply see if the server is active.
* The return value is a JSON object.
* FEEDBACK: return 0 for success, non-zero for error
*/

var http = require('http');
//get host IP and port # from command line
var myArgs = process.argv.slice(2);
HOST_IP = myArgs[0];
PORT_NUM = myArgs[1];
SUCCESS = 0

/*
request options JSON
host - IP address of server computer
port - port where service is running
path - route of service you are trying to reach
method - type of HTTP service
*/
var options = {
  'host': HOST_IP,
  'port': PORT_NUM,
  'path': '/hello',     
  'method': 'GET'
};

//send the GET request to get the vote count
var req = http.request(options, readResponse);
req.on('error', function(err) {
  console.log('ERR: ' + err);
  console.log('Error caught: ' + err.errno); //non-zero is an error code
  process.exit(err.errno);
});
req.end();

function readResponse(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
    console.log(responseData);
    obj = JSON.parse(responseData);
    if (obj.msg == 'Hi ') {
      console.log('SUCCESS');
      process.exit(SUCCESS); //0 is success
    } else {
      console.log('ERROR');
      process.exit(-1); //-1 is error
    }
  });
}
