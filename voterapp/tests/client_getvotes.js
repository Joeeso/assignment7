/*
* This program calls the getVotes GET request to get the current vote count.
* The return value is a JSON object.
* FEEDBACK: return 0 for success, non-zero for error
*/

var http = require('http');

//get host IP and port # from command line
var myArgs = process.argv.slice(2);
HOST_IP = myArgs[0];
PORT_NUM = myArgs[1];
SUCCESS = 0

/*
request options JSON
host - IP address of server computer
port - port where service is running
path - route of service you are trying to reach
method - type of HTTP service
*/
var options = {
  'host': HOST_IP,
  'port': PORT_NUM,
  'path': '/getVotes',
  'method': 'GET'
};


//send the GET request to get the vote count
var req = http.request(options, readResponse);
req.on('error', function(err) {
  console.log('ERR: ' + err);
  console.log('Error caught: ' + err.errno); //non-zero is an error code
  process.exit(err.errno);
});
req.end();

function readResponse(response) {
  var responseData = '';
  response.on('data', function (chunk) {
    responseData += chunk;
  });
  response.on('end', function() {
    console.log(responseData);
    obj = JSON.parse(responseData); //{"num_skittles":0,"num_mms":0}

    //votes can't be negative
    if ( obj.num_skittles < 0 || obj.num_mms < 0 ) {
      console.log('ERROR'); 
      process.exit(-1); //-1 is error
    } else {
      console.log('SUCCESS'); 
      process.exit(SUCCESS); //0 is success
    }
  });
}
