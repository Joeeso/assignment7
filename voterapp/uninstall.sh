#!/bin/bash

# exit code 0 - success, nonzero - error

echo "*** An uninstallation script for the Voter App ***"

# begin uninstallation

echo "stopping services..."
pm2 stop all  >/dev/null 2>&1

echo "deleting services..."
pm2 delete all >/dev/null 2>&1

echo "removing Node.js libraries"
rm -rf node_modules >/dev/null 2>&1

echo "removing log files"
rm logs/*.log >/dev/null 2>&1

echo "Uninstalling pm2..."
npm uninstall -g pm2 >/dev/null 2>&1

echo "Uninstallation COMPLETE!"
