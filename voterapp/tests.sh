#!/bin/bash

# FEEDBACK: exit code 0 - success, nonzero - error

echo "*** A test script for the Voter App ***"

# pass in HOST IP and PORT NUMBER

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <HOST IP> <PORT>"
    exit 99
fi
HOST_IP=$1
PORT=$2

# TEST 1
cmd="node tests/client_ping.js $HOST_IP $PORT"
$cmd >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 1 failed"
    exit $rc
else
    echo "TEST 1 success!"
fi

# TEST 2
cmd="node tests/client_hello.js $HOST_IP $PORT"
$cmd  >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 2 failed"
    exit $rc
else
    echo "TEST 2 success!"
fi

# TEST 3
cmd="node tests/client_helloparam.js $HOST_IP $PORT"
$cmd >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 3 failed"
    exit $rc
else
    echo "TEST 3 success!"
fi

# TEST 4
cmd="node tests/client_vote.js $HOST_IP $PORT"
$cmd >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 4 failed"
    exit $rc
else
    echo "TEST 4 success!"
fi

# TEST 5
cmd="node tests/client_getvotes.js $HOST_IP $PORT"
$cmd >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 5 failed"
    exit $rc
else
    echo "TEST 5 success!"
fi

# TEST 6 - use curl to test a URL directly
http_status=$(curl --write-out %{http_code} --silent --output /dev/null http://$HOST_IP:$PORT/index.html )
if [ "$http_status" -ne 200 ]; then
    echo "TEST 6 failed"
    exit $rc
else
    echo "TEST 6 success!"
fi

# TEST 7
cmd="node tests/client_foo.js $HOST_IP $PORT"
$cmd >/dev/null 2>&1
rc=$?
if [ "$rc" -ne 0 ]; then
    echo "TEST 7 failed"
    exit $rc
else
    echo "TEST 7 success!"
fi




# if we make it here, all tests passed; SUCCESS
exit 0

