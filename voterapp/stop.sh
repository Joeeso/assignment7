#!/bin/bash

# exit code 0 - success, nonzero - error

echo "*** A STOP script for the Voter App ***"

pm2 stop all
