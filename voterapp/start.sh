#!/bin/bash

# exit code 0 - success, nonzero - error

echo "*** A START script for the Voter App ***"

pm2 stop all
pm2 delete all
pm2 start process.json
sleep 1
echo "Checking status..."
sleep 2
pm2 status