#!/bin/bash

# exit code 0 - success, nonzero - error

echo "*** An installation script for the Voter App ***"

# check if Node exists
cmd="node -v"
$cmd
status=$?
if [ "$status" -eq 0 ]; then
    echo "Success - found Node.js..."
else
   echo "ERROR - Please install Node.js before running this script"
   exit $status
fi

# begin installation

# may need root permissions (sudo) when running from command line
# sudo npm install -g pm2   
# but from CI/CD, it is fine like this
npm install -g pm2  

# install node libraries for our website
npm install

echo "Installation Complete!"
