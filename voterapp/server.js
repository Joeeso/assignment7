//required modules
var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//create server variables to store the votes
var num_skittles = 0;
var num_mms = 0;

var config = require('./config'); //config.js file in root
var port = config.server.port;

// configure the web server app
app.use(bodyParser.json());
app.use(express.static('./')); //look in current folder for .html files
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({type: 'application/vnd/api+json'}));

// create the routes

app.get('/ping', function(req, res) {
  var responseJson = {"msg": "pong"};
  res.status(200).send(responseJson);
  return;
});

app.get('/foo', function(req, res) {
  var responseJson = {"msg": "bar"};
  res.status(200).send(responseJson);
  return;

app.get('/hello', function(req, res) {
  var name = req.query.name;
  if (!name)
    name = "";
  var responseJson = {"msg": "Hi " + name};
  res.status(200).send(responseJson);
  return;
});

app.get('/getVotes', function(req, res) {
  var responseJson = { "num_skittles" : num_skittles , "num_mms" : num_mms };
  res.status(200).send(responseJson);
  return;
});

app.post('/vote',function(req, res) {
  var the_vote = req.body.vote;
  the_vote = the_vote.toLowerCase().trim();
  switch(the_vote) {
    case "skittles":
      num_skittles++;
    break;
    case "m&ms":
      num_mms++;
    break;
    default:
      res.status(200).send({"msg":"invalid vote: " + the_vote});
      return;
  }
  res.status(200).send({"msg":"Thank you for voting for: " + the_vote + "!"});
  return;
});

// start the web server
var httpServer = http.createServer(app);
httpServer.listen(port);
console.log('server running: http://localhost:%s/  (Ctrl+C to Quit)', port);
