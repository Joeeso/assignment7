# Get out and vote!

## Skittles vs. M&Ms

This server accepts real-time web votes for the favorite candy: Skittles or M&Ms.

### Prerequisites:
Here are the prerequisites for the server host. You must perform these steps on your computer:
- [Node.js LTS](https://nodejs.org/en/)
- This *voterapp* folder

### Installation
Install the application software:
```sh
$ npm install
$ sudo npm install -g pm2
```

### Configuration
Application configuration, edit `voterapp/config.js` Runtime configuration, edit `voterapp/process.json`

### Deployment
```sh
$ cd voterapp  # go to the voterapp folder
$ pm2 start process.json  # start servers and add to pm2 management
$ pm2 status  # check status
$ pm2 stop all  # stop all servers
$ pm2 start all  # start the servers again
$ pm2 delete all  # delete servers from pm2 management
```

### Testing


The tests folder has Node.js HTTP clients to test the web server.

Test scripts return `0` (SUCCESS) or **non-zero** (ERROR).

The examples below assume that the server is running at IP `127.0.0.1` and port `1234`:

```sh
$  node client_ping.jstyt 127.0.0.1 12347 
{"msg":"pong"}

$  node client_hello.js 127.0.0.1 1234
{"msg":"Hi "}

$  node client_helloparam.js 127.0.0.1 1234
{"msg":"Hi Alice"}

$  node client_vote.js 127.0.0.1 1234
Complete response: Thank you for voting for: skittles!

$  node client_getvotes.js 127.0.0.1 1234
{"num_skittles":1,"num_mms":0}
```

### Automation
The following BASH scripts automate the manual tasks above (excluding the prerequisites):
```sh
install.sh
start.sh
tests.sh
stop.sh
uninstall.sh
```
